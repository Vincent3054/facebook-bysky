﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotnetWebApi0401.Resources.Request;
using Models;
using DotnetWebApi0401.Resources.Response;


namespace DotnetWebApi0401.Services
{
    public class MembersService
    {
        private Dictionary<string, Members> _members = new Dictionary<string, Members>();
        public Members loginMembers =new Members();

        public async Task<bool> LoginMembers(LoginMembers logindata)
        {
            Members member = await GetMembers(logindata.Account);
            if (member.Account == logindata.Account && member.Password==logindata.Password )
            {
                loginMembers.Account = member.Account;
                loginMembers.Password = member.Password;
                loginMembers.M_Id = member.M_Id;
                return true; 
            }
            return false;
        }
        public async Task<bool> LogoutMembers()
        {
            try
            {
                loginMembers.Account = string.Empty;
                loginMembers.Password = string.Empty;
                loginMembers.M_Id = string.Empty;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        
        public async Task<List<Members>> GetAllMembers()
        {
            var data = new List<Members>();
            foreach (var item in _members)
            {
                data.Add(item.Value);
            }
            return data;
        }
        public async Task<Members> GetMembers(string Account)
        {
            var data = new Members();
            string M_Id = AccountGetMid(Account);
            if (!string.IsNullOrWhiteSpace(M_Id))
            {
                if (_members.ContainsKey(M_Id))
                {
                    data = _members[M_Id];
                    return data;
                }
            }
            return data;
        }
        public async Task<bool> AddMembers(AddMembers membersdata)
        {
            var data = new Members();
            if (AccountGetMid(membersdata.Account)==null)
            {
                data.M_Id = Guid.NewGuid().ToString();
                data.Account = membersdata.Account;
                data.Password = membersdata.Password;
                data.Name = membersdata.Name;
                var Friend = new List<string>() ;
                data.Friend = Friend;
                _members.Add(data.M_Id, data);
                return true;
            }
            return false;
        }
        public string AccountGetMid(string Account)
        {
            foreach (var item in _members)
            {
                if (Account == item.Value.Account)
                {
                    return item.Key;
                }
            }
            return null;
        }
        public async Task<bool> EditMembers(string Account, EditMembers membersdata)
        {
            var data = new Members();
            string M_Id = AccountGetMid(Account);
            if (_members.ContainsKey(M_Id))
            {
                data.M_Id = M_Id;
                data.Account = membersdata.Account;
                data.Password = membersdata.Password;
                data.Name = membersdata.Name;
                _members[M_Id] = data;
                return true;
            }
            return false;
        }

        public async Task<bool> DeleteMembers(string Account)
        {
            try
            {
                string M_Id = AccountGetMid(Account);
                if (_members.ContainsKey(M_Id))
                {
                    _members.Remove(M_Id);
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return false;
        }
        public async Task<bool> AddFriendMembers(string friendAccount)
        {
            string Account = loginMembers.Account;
            string M_Id = loginMembers.M_Id;
            if (string.IsNullOrWhiteSpace(Account)|| string.IsNullOrWhiteSpace(M_Id))
            {
                return false;//未登入
            }
            try
            {
                var data = await GetMembers(Account);
                var friendNum = 0;
                //確認Members字典裡面  mid 有沒有重複第二筆
                foreach (var item in data.Friend)
                {
                    if (item == friendAccount)
                    {
                        friendNum++;
                    }
                }
                //已有資料 不新增 
                if (friendNum > 0)
                {
                    return false;
                }
                else
                {
                    data.Friend.Add(friendAccount);
                    _members[M_Id]= data;
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public async Task<bool> DeleteFriendMembers(string friendAccount)
        {
            string Account = loginMembers.Account;
            string M_Id = loginMembers.M_Id;
            if (string.IsNullOrWhiteSpace(Account) || string.IsNullOrWhiteSpace(M_Id))
            {
                return false;//未登入
            }
            try
            {
                if (_members.ContainsKey(M_Id))
                {
                    var data = _members[M_Id];
                    for (int i = 0; i < data.Friend.Count; i++)
                    {
                        if (data.Friend[i] == friendAccount && data.Friend.Count == 1)
                        {
                            List<string> emptyFriend = new List<string>();
                            data.Friend = emptyFriend;
                        }
                        else if (data.Friend[i] == friendAccount)
                        {
                            data.Friend[i] = string.Empty;
                        }
                    }
                    _members[M_Id] = data;
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return false;
        }
    }
}
