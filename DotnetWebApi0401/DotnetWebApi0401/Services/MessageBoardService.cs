﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotnetWebApi0401.Resources.Request;
using Models;
using DotnetWebApi0401.Resources.Response;


namespace DotnetWebApi0401.Services
{
    public class MessageBoardService
    {
        private Dictionary<string, MessageBoard> _messageboard = new Dictionary<string, MessageBoard>();
        private Dictionary<string, ReplyMessageBoard> _replymessageboard = new Dictionary<string, ReplyMessageBoard>();
        private Dictionary<string, LikeMessageBoard> _likemessageboard = new Dictionary<string, LikeMessageBoard>();

        #region 文章
        public async Task<List<MessageBoard>> GetAllMessageBoard()
        {
            var data = new List<MessageBoard>();
            foreach (var item in _messageboard)
            {
                data.Add(item.Value);
            }
            return data;
        }
        public async Task<List<MessageBoard>> GetAllMessageBoardBymid(string M_Id)
        {
            var data = new List<MessageBoard>();
            foreach (var item in _messageboard)
            {
                if (item.Value.M_Id == M_Id)
                {
                    data.Add(item.Value);
                }
            }
            return data;
        }
        public async Task<MessageBoard> GetMessageBoard(string C_Id)
        {
            var data = new MessageBoard();

            if (_messageboard.ContainsKey(C_Id))
            {
                data = _messageboard[C_Id];
            }

            return data;
        }
        public async Task<bool> AddMessageBoard(string M_Id, PostMessageBoard MessageBoarddata)
        {
            try
            {
                List<string> emptyL_Id = new List<string>();
                List<string> emptyR_Id = new List<string>();
                var data = new MessageBoard();
                data.M_Id = M_Id;
                data.C_Id = Guid.NewGuid().ToString();
                data.L_Id = emptyL_Id;
                data.R_Id = emptyR_Id;
                data.Content = MessageBoarddata.Content;
                data.CreateTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                _messageboard.Add(data.C_Id, data);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<bool> EditMessageBoard(string C_Id, PostMessageBoard MessageBoarddata)
        {
            var data = new MessageBoard();
            if (_messageboard.ContainsKey(C_Id))
            {
                data = await GetMessageBoard(C_Id);
                data.C_Id = C_Id;
                data.M_Id = data.M_Id;
                data.R_Id = data.R_Id;
                data.L_Id = data.L_Id;
                data.Content = MessageBoarddata.Content;
                data.CreateTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                _messageboard[C_Id] = data;
                return true;
            }
            return false;
        }

        public async Task<bool> DeleteMessageBoard(string C_Id)
        {
            if (_messageboard.ContainsKey(C_Id))
            {
                _messageboard.Remove(C_Id);
                return true;
            }
            return false;
        }
        #endregion


        #region 回復
        public async Task<ReplyMessageBoard> GetReplyMessageBoard(string R_Id)
        {
            var replyData = new ReplyMessageBoard();

            if (_replymessageboard.ContainsKey(R_Id))
            {
                replyData = _replymessageboard[R_Id];
            }

            return replyData;
        }
        public async Task<bool> AddReplyMessageBoard(string M_Id, string C_ID, PostReplyMessageBoard postReplyMessageBoarddata)
        {
            try
            {
                //新增回覆資料
                var replyData = new ReplyMessageBoard();
                replyData.M_Id = M_Id;
                replyData.C_Id = C_ID;
                replyData.R_Id = Guid.NewGuid().ToString();
                replyData.Reply = postReplyMessageBoarddata.Reply;
                replyData.ReplyDateTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                _replymessageboard.Add(replyData.R_Id, replyData);
                //把R_ID新增到相應的C_ID裡面
                var data = await GetMessageBoard(replyData.C_Id);
                data.R_Id.Add(replyData.R_Id);
                _messageboard[replyData.C_Id] = data;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public async Task<bool> EditReplyMessageBoard(string R_Id, PostReplyMessageBoard postReplyMessageBoarddata)
        {
            var replyData = new ReplyMessageBoard();
            if (_replymessageboard.ContainsKey(R_Id))
            {
                replyData = await GetReplyMessageBoard(R_Id);
                replyData.Reply = postReplyMessageBoarddata.Reply;
                replyData.ReplyDateTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                _replymessageboard[R_Id] = replyData;
                return true;
            }
            return false;
        }
        public async Task<bool> DeleteReplyMessageBoard(string R_Id)
        {
            if (_replymessageboard.ContainsKey(R_Id))
            {

                //刪除_messageboard裡面的R_Id
                var replyData = await GetReplyMessageBoard(R_Id);
                var cData = await GetMessageBoard(replyData.C_Id);

                for (int i = 0; i < cData.R_Id.Count; i++)
                {
                    if (cData.R_Id[i] == R_Id && cData.R_Id.Count == 1)
                    {
                        List<string> emptyR_Id = new List<string>();
                        cData.R_Id = emptyR_Id;
                    }
                    else if (cData.R_Id[i] == R_Id)
                    {
                        cData.R_Id[i] = string.Empty;
                    }
                }
                _messageboard[replyData.C_Id] = cData;
                //刪除_replymessageboard裡面的資料
                _replymessageboard.Remove(R_Id);
                return true;
            }
            return false;
        }


        #endregion

        #region 喜歡or不喜歡
        public async Task<List<LikeMessageBoard>> GetAllLickMessageBoard()
        {
            var data = new List<LikeMessageBoard>();
            foreach (var item in _likemessageboard)
            {
                data.Add(item.Value);
            }
            return data;
        }
        public async Task<LikeMessageBoard> GetLickMessageBoard(string L_Id)
        {
            var likeData = new LikeMessageBoard();

            if (_likemessageboard.ContainsKey(L_Id))
            {
                likeData = _likemessageboard[L_Id];
            }
            return likeData;
        }
        public async Task<bool> PostLikeMessageBoard(string M_Id, string C_Id)
        {
            //確認Like字典裡面 CID 和 mid 有沒有重複第二筆
            //一個人同一個文章只能新增一次 裡面如果經有一樣的MID&&CID不給新增LID
            var repeatNum = 0;
            var oldLid = "";
            foreach (var data in _likemessageboard)
            {
                if (data.Value.C_Id == C_Id && data.Value.M_Id == M_Id)
                {
                    repeatNum++;
                    oldLid = data.Key;//代改
                }
            }
            //(Edit)已有資料 僅修改(不新增LID) 
            if (repeatNum > 0)
            {
                try
                {
                    var likeData = new LikeMessageBoard();
                    if (_likemessageboard.ContainsKey(oldLid))
                    {
                        likeData = await GetLickMessageBoard(oldLid);
                        //如果沒點過讚，或是取消了讚。 -> 讚True
                        //如果點過讚。 ->取消讚False
                        if (likeData.Like == null || likeData.Like == false)
                        {
                            likeData.Like = true;
                        }
                        else
                        {
                            likeData.Like = false;
                        }
                        likeData.LikeTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                        _likemessageboard[oldLid] = likeData;
                        return true;
                    }
                    return false;
                }
                catch
                {
                    return false;
                }
            }
            //(Add)沒資料 新增Lid 
            else
            {
                try
                {
                    //新增喜歡資料
                    var likeData = new LikeMessageBoard();
                    likeData.L_Id = Guid.NewGuid().ToString();
                    likeData.M_Id = M_Id;
                    likeData.C_Id = C_Id;
                    
                    if (likeData.Like == null || likeData.Like == false)
                    {
                        likeData.Like = true;
                    }
                    else
                    {
                        likeData.Like = false;
                    }
                    likeData.LikeTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                    _likemessageboard.Add(likeData.L_Id, likeData);

                    //把L_ID新增到相應的C_ID裡面
                    var data = await GetMessageBoard(likeData.C_Id);
                    data.L_Id.Add(likeData.L_Id);
                    _messageboard[likeData.C_Id] = data;
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }

        }
        //public async Task<bool> EditLikeMessageBoard(string L_Id, PostLikeMessageBoard postLikeMessageBoarddata)
        //{
        //    var likeData = new LikeMessageBoard();
        //    if (_likemessageboard.ContainsKey(L_Id))
        //    {
        //        likeData = await GetLickMessageBoard(L_Id);
        //        likeData.Like = postLikeMessageBoarddata.Like;
        //        likeData.LikeTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        //        _likemessageboard[L_Id] = likeData;
        //        return true;
        //    }
        //    return false;
        //}

        public async Task<bool> DeleteLikeMessageBoard(string L_Id)
        {
            if (_likemessageboard.ContainsKey(L_Id))
            {
                _likemessageboard.Remove(L_Id);
                return true;
            }
            return false;
            //缺刪除List裡面的id
        }
        #endregion
    }
}
