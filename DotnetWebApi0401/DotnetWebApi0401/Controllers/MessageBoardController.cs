﻿using DotnetWebApi0401.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotnetWebApi0401.Resources.Request;
using DotnetWebApi0401.Resources.Response;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DotnetWebApi0401.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class MessageBoardController : ControllerBase
    {
        private readonly MembersService _msvc;
        private readonly MessageBoardService _mbsvc;
        //建構式接收IDataService
        public MessageBoardController(MessageBoardService mbsvc , MembersService msvc)
        {
            _mbsvc = mbsvc;
            _msvc = msvc;
        }
        #region 文章
        /// <summary>
        /// 查詢所有文章
        /// </summary>
        // GET api/<MessageBoardController>/GetAllMessageBoard
        [HttpGet("GetAllMessageBoard")]
        public async Task<ActionResult> GetAllMessageBoard()
        {
            try
            {
                List<MessageBoard> MessageBoard  = await _mbsvc.GetAllMessageBoard();
                return Ok(new ResultList<MessageBoard>(true, 200, "查詢成功", null, MessageBoard));
            }
            catch
            {
                return NotFound(new Result(false, 404, "查詢失敗")); //400
            }

        }
        /// <summary>
        /// 查詢文章(用C_Id)
        /// </summary>
        // GET api/<MessageBoardController>/GetMessageBoardBycid
        [HttpGet("GetMessageBoardBycid/{C_Id}")]
        public async Task<ActionResult> GetMessageBoardBycid(string C_Id)
        {
            try
            {
                MessageBoard message = await _mbsvc.GetMessageBoard(C_Id);
                if (message.C_Id != null)
                {
                    return Ok(new Result<MessageBoard>(true, 200, "查詢成功", null, message));
                }
                else
                {
                    return BadRequest(new Result(false, 404, "ID不存在")); //400
                }
            }
            catch
            {
                return NotFound(new Result(false, 404, "查詢失敗")); //400
            }

        }
        /// <summary>
        /// 查詢特定使用者發的文章(用M_Id)
        /// </summary>
        // GET api/<MessageBoardController>/GetMessageBoardBycid
        [HttpGet("GetAllMessageBoardBymid/{M_Id}")]
        public async Task<ActionResult> GetAllMessageBoardBymid(string M_Id)
        {
            try
            {
                List<MessageBoard> MessageBoard = await _mbsvc.GetAllMessageBoardBymid(M_Id);
                return Ok(new ResultList<MessageBoard>(true, 200, "查詢成功", null, MessageBoard));
            }
            catch
            {
                return NotFound(new Result(false, 404, "查詢失敗")); //400
            }

        }
        // <summary>
        /// 新增文章
        /// </summary>
        // POST: api/MessageBoard/AddMessageBoard
        [HttpPost("AddMessageBoard")]
        public async Task<ActionResult>  AddMessageBoard(PostMessageBoard content)
        {
            string M_Id = _msvc.loginMembers.M_Id;

            if (string.IsNullOrWhiteSpace(M_Id))
            {
                return BadRequest(new Result(false, 400, "請先登入"));
            }
            if (await _mbsvc.AddMessageBoard(M_Id,content))
            {
                return Ok(new Result(true, 200, "新增成功"));
            }
            else
            {
                return BadRequest(new Result(false, 400, "新增失敗"));
            }
        }
        // <summary>
        /// 修改文章
        /// </summary>
        // PUT api/MessageBoard/PostMessageBoard
        [HttpPut("EditMessageBoard/{C_Id}")]
        public async Task<ActionResult> EditMessageBoard(string C_Id, PostMessageBoard content)
        {
            MessageBoard message = await _mbsvc.GetMessageBoard(C_Id);
            string M_Id = _msvc.loginMembers.M_Id;
            if (message.M_Id != M_Id)
            {
                return BadRequest(new Result(false, 400, "發文者才能編輯文章"));
            }
            if (string.IsNullOrWhiteSpace(M_Id))
            {
                return BadRequest(new Result(false, 400, "請先登入"));
            }
            if (await _mbsvc.EditMessageBoard(C_Id, content))
            {
                return Ok(new Result(true, 200, "修改成功"));
            }
            else
            {
                return BadRequest(new Result(false, 400, "修改失敗"));
            }
        }
        // <summary>
        /// 刪除文章
        /// </summary>
        // Delete api/DeleteMessageBoard/{C_Id}
        [HttpDelete("DeleteMessageBoard/{C_Id}")]
        public async Task<ActionResult> DeleteMessageBoard(string C_Id)
        {
            if (await _mbsvc.DeleteMessageBoard(C_Id))
            {
                return Ok(new Result(true, 200, "刪除成功"));
            }
            else
            {
                return BadRequest(new Result(false, 400, "刪除失敗"));
            }
        }
        
        /// <summary>
        /// 查詢文章(用R_Id)
        /// </summary>
        // GET api/<MessageBoardController>/GetReplyMessageBoardByrid/{R_Id}
        [HttpGet("GetReplyMessageBoardByrid/{R_Id}")]
        public async Task<ActionResult> GetReplyMessageBoardBycid(string R_Id)
        {
            try
            {
                ReplyMessageBoard message = await _mbsvc.GetReplyMessageBoard(R_Id);
                if (message.R_Id != null)
                {
                    return Ok(new Result<ReplyMessageBoard>(true, 200, "查詢成功", null, message));
                }
                else
                {
                    return BadRequest(new Result(false, 404, "ID不存在")); //400
                }
            }
            catch
            {
                return NotFound(new Result(false, 404, "查詢失敗")); //400
            }

        }
        #endregion
        #region 回覆
        // <summary>
        /// 新增回覆
        /// </summary>
        // POST: api/MessageBoard/AddReplyMessageBoard/{C_Id}
        [HttpPost("AddReplyMessageBoard/{C_Id}")]
        public async Task<ActionResult> AddReplyMessageBoard(string C_ID ,PostReplyMessageBoard content)
        {
            string M_Id = _msvc.loginMembers.M_Id;
            
            if (await _mbsvc.AddReplyMessageBoard(M_Id, C_ID, content))
            {
                return Ok(new Result(true, 200, "新增成功"));
            }
            else
            {
                return BadRequest(new Result(false, 400, "新增失敗"));
            }
        }
        // <summary>
        /// 修改回覆
        /// </summary>
        // PUT api/MessageBoard/EditReplyMessageBoard/{R_Id}
        [HttpPut("EditReplyMessageBoard/{R_Id}")]
        public async Task<ActionResult> EditReplyMessageBoard(string R_Id, PostReplyMessageBoard content)
        {
            if (await _mbsvc.EditReplyMessageBoard(R_Id, content))
            {
                return Ok(new Result(true, 200, "修改成功"));
            }
            else
            {
                return BadRequest(new Result(false, 400, "修改失敗"));
            }
        }
        // <summary>
        /// 刪除回覆
        /// </summary>
        // Delete api/DeleteReplyMessageBoard/{R_Id}
        [HttpDelete("DeleteReplyMessageBoard/{R_Id}")]
        public async Task<ActionResult> DeleteReplyMessageBoard(string R_Id)
        {
            if (await _mbsvc.DeleteReplyMessageBoard(R_Id))
            {
                return Ok(new Result(true, 200, "刪除成功"));
            }
            else
            {
                return BadRequest(new Result(false, 400, "刪除失敗"));
            }
        }

        #endregion
        #region 按讚
        /// <summary>
        /// 查詢喜歡狀態(用L_ID)
        /// </summary>
        // GET api/<MessageBoardController>/GetPostLikeMessageBoardBylid/{L_Id}
        [HttpGet("GetPostLikeMessageBoardBylid/{L_Id}")]
        public async Task<ActionResult> GetPostLikeMessageBoardBylid(string L_Id)
        {
            try
            {
                LikeMessageBoard message = await _mbsvc.GetLickMessageBoard(L_Id);
                if (message.L_Id != null)
                {
                    return Ok(new Result<LikeMessageBoard>(true, 200, "查詢成功", null, message));
                }
                else
                {
                    return BadRequest(new Result(false, 404, "ID不存在")); //400
                }
            }
            catch
            {
                return NotFound(new Result(false, 404, "查詢失敗")); //400
            }

        }

        // <summary>
        /// 新增或收回喜歡
        /// </summary>
        // POST: api/MessageBoard/PostLikeMessageBoard/{C_Id}
        [HttpPost("PostLikeMessageBoard/{C_Id}")]
        public async Task<ActionResult> PostLikeMessageBoard(string C_ID)
        {
            string M_Id = _msvc.loginMembers.M_Id;

            if (await _mbsvc.PostLikeMessageBoard(M_Id, C_ID))
            {
                return Ok(new Result(true, 200, "新增成功"));
            }
            else
            {
                return BadRequest(new Result(false, 400, "新增失敗"));
            }
        }
        /// 刪除喜歡L_Id
        /// </summary>
        // Delete api/DeleteLikeMessageBoard/{L_Id}
        [HttpDelete("DeleteLikeMessageBoard/{L_Id}")]
        public async Task<ActionResult> DeleteLikeMessageBoard(string L_Id)
        {
            if (await _mbsvc.DeleteLikeMessageBoard(L_Id))
            {
                return Ok(new Result(true, 200, "刪除成功"));
            }
            else
            {
                return BadRequest(new Result(false, 400, "刪除失敗"));
            }
        }
        #endregion

    }
}
