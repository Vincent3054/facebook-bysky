﻿using DotnetWebApi0401.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotnetWebApi0401.Resources.Request;
using DotnetWebApi0401.Resources.Response;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DotnetWebApi0401.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class MembersController : ControllerBase
    {
        private readonly MembersService _msvc;
        //建構式接收IDataService
        public MembersController(MembersService msvc)
        {
            _msvc = msvc;
        }
        /// <summary>
        /// 登入會員
        /// </summary>
        // Post api/<MembersController>/Login
        [HttpPost("Login")]
        public async Task<ActionResult> Login(LoginMembers logindata)
        {
            try
            {
                if (await _msvc.LoginMembers(logindata))
                {
                    return Ok(new Result(true, 200, "登入成功"));
                }
                else
                {
                    return BadRequest(new Result(false, 404, "登入失敗")); //400
                }
            }
            catch
            {
                return NotFound(new Result(false, 404, "登入錯誤")); //400
            }

        }
        /// <summary>
        /// 登出會員
        /// </summary>
        // Post api/<MembersController>/Logout
        [HttpPost("Logout")]
        public async Task<ActionResult> Logout()
        {
            try
            {
                if (await _msvc.LogoutMembers())
                {
                    return Ok(new Result(true, 200, "登出成功"));
                }
                else
                {
                    return BadRequest(new Result(false, 404, "登出失敗")); //400
                }
            }
            catch
            {
                return NotFound(new Result(false, 404, "登出錯誤")); //400
            }

        }
        /// <summary>
        /// 查詢所有會員
        /// </summary>
        // GET api/<MembersController>/GetAllMembers
        [HttpGet("GetAllMembers")]
        public async Task<ActionResult> GetAllMembers()
        {
            try
            {
                List<Members> members  = await _msvc.GetAllMembers();
                return Ok(new ResultList<Members>(true, 200, "查詢成功", null, members));
            }
            catch
            {
                return NotFound(new Result(false, 404, "查詢失敗")); //400
            }

        }
        /// <summary>
        /// 查詢會員
        /// </summary>
        // GET api/<MembersController>/GetMembers
        [HttpGet("GetMembers/{Account}")]
        public async Task<ActionResult> GetMembers(string Account)
        {
            try
            {
                Members member = await _msvc.GetMembers(Account);
                if (member.M_Id != null)
                {
                    return Ok(new Result<Members>(true, 200, "查詢成功", null, member));
                }
                else
                {
                    return BadRequest(new Result(false, 404, "ID不存在")); //400
                }
            }
            catch
            {
                return NotFound(new Result(false, 404, "查詢失敗")); //400
            }

        }
        // <summary>
        /// 新增會員
        /// </summary>
        // POST: api/Members/AddMembers
        [HttpPost("AddMembers")]
        public async Task<ActionResult>  AddMembers(AddMembers membersdata)
        {
            if (await _msvc.AddMembers(membersdata))
            {
                return Ok(new Result(true, 200, "新增成功"));
            }
            else
            {
                return BadRequest(new Result(false, 400, "新增失敗"));
            }
        }
        // <summary>
        /// 修改會員
        /// </summary>
        // PUT api/Members/EditMembers/{Account}
        [HttpPut("EditMembers/{Account}")]
        public async Task<ActionResult> EditMembers(string Account, EditMembers membersdata)
        {
            if (await _msvc.EditMembers(Account, membersdata))
            {
                return Ok(new Result(true, 200, "修改成功"));
            }
            else
            {
                return BadRequest(new Result(false, 400, "修改失敗"));
            }
        }
        // <summary>
        /// 刪除會員
        /// </summary>
        // Delete api/DeleteMembers/{Account}
        [HttpDelete("DeleteMembers/{Account}")]
        public async Task<ActionResult> DeleteMembers(string Account)
        {
            if (await _msvc.DeleteMembers(Account) )
            {
                return Ok(new Result(true, 200, "刪除成功"));
            }
            else
            {
                return BadRequest(new Result(false, 400, "刪除失敗"));
            }
        }
        // <summary>
        /// 新增好友
        /// </summary>
        // POST: api/Members/AddFriendMembers/{friendAccount}
        [HttpPost("AddFriendMembers/{friendAccount}")]
        public async Task<ActionResult> AddFriendMembers(string friendAccount)
        {
            if (await _msvc.AddFriendMembers(friendAccount))
            {
                return Ok(new Result(true, 200, "新增成功"));
            }
            else
            {
                return BadRequest(new Result(false, 400, "新增失敗，或未登入"));
            }
        }
        // <summary>
        /// 刪除好友
        /// </summary>
        // Delete api/DeleteFriendMembers/{friendAccount}
        [HttpDelete("DeleteFriendMembers/{friendAccount}")]
        public async Task<ActionResult> DeleteFriendMembers(string friendAccount)
        {
            if (await _msvc.DeleteFriendMembers(friendAccount))
            {
                return Ok(new Result(true, 200, "刪除成功"));
            }
            else
            {
                return BadRequest(new Result(false, 400, "刪除失敗，或未登入"));
            }
        }
    }
}
