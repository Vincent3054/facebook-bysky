﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotnetWebApi0401.Resources.Request
{
    public class MemberModel
    {
        //會員編號(主鍵)
        public string M_Id { get; set; }
        //帳號
        public string Account { get; set; }
        //密碼
        public  string Password { get; set; }
        //信箱
        public string Email { get; set; }
        //姓名
        public  string Name { get; set; }
        //性別
        public string Sex { get; set; }
        //生日
        public DateTime BirthDate { get; set; }
        //創建日期
        public DateTime CreateTime { get; set; }

    }
}
