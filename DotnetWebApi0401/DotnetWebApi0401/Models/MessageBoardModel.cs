﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    public class MessageBoardModel
    {
        //會員編號
        public string M_Id { get; set; }
        //文件編號
        public string C_Id { get; set; }
        //文件內容
        public string Content { get; set; }
        //回覆編號
        public string R_Id { get; set; }
        //回覆內容
        public string Reply { get; set; }
        //喜歡
        public string LikeName { get; set; }
        //創建日期
        public DateTime CreateTime { get; set; }

    }
}
