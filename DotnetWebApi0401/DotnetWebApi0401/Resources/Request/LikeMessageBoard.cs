﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotnetWebApi0401.Resources.Request
{
    public class LikeMessageBoard
    {
        public  string M_Id { get; set; }
        public string C_Id { get; set; }
        public string L_Id { get; set; }
        public bool Like { get; set; }
        public  string LikeTime { get; set; }
    }
}
