﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotnetWebApi0401.Resources.Request
{
    public class LoginMembers
    {
        //帳號
        public string Account { get; set; }
        //密碼
        public string Password { get; set; }
    }
}
