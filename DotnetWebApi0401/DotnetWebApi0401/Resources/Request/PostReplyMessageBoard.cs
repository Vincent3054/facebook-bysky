﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotnetWebApi0401.Resources.Request
{
    public class PostReplyMessageBoard
    {
        public  string Reply { get; set; }
    }
}
