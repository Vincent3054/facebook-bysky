﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotnetWebApi0401.Resources.Request
{
    public class Members
    {
        //會員編號(主鍵)
        public string M_Id { get; set; }
        //帳號
        public string Account { get; set; }
        //密碼
        public string Password { get; set; }
        //姓名
        public string Name { get; set; }
        //好友
        public List<string> Friend { get; set; }
    }
}
