﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotnetWebApi0401.Resources.Request
{
    public class MessageBoard
    {
        //會員編號
        public string M_Id { get; set; }
        //文件編號
        public string C_Id { get; set; }
        //文件內容
        public string Content { get; set; }
        //回覆編號
        public List<string> R_Id { get; set; }
        //回覆內容
        //public string Reply { get; set; }
        //喜歡ID
        public List<string> L_Id { get; set; }
        //喜歡的人
        //public string LikeName { get; set; }
        //創建日期
        public string CreateTime { get; set; }

    }
}
