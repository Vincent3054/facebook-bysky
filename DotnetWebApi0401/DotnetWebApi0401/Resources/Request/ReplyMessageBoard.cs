﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotnetWebApi0401.Resources.Request
{
    public class ReplyMessageBoard
    {
        public  string M_Id { get; set; }
        public  string R_Id {get; set; }
        public string C_Id { get; set; }
        public string Reply { get; set; }
        public  string ReplyDateTime { get; set; }
    }
}
